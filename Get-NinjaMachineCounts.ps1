#Import the PoSHNinjaRMM PowerShell Module (Download from ----> https://bitbucket.org/MaxAnderson95/posh-ninja-rmm/overview)
Import-Module .\PoSHNinjaRMM.psm1

#Get a list of Devices from the NinjaAPI
$Devices = Get-NinjaDevice

#Get a list of Customers from the NinjaAPI
$Customers = Get-NinjaCustomer

#Create a blank array
$FinalOutput = @()

#Loop through each customer
ForEach ($Customer in $Customers) {
    
    #Get a count of Workstaions
    $WorkstationCount = $Devices | Where-Object {$_.customer_id -eq $customer.id -and $_.role -eq "WINDOWS_WORKSTATION"} | Measure-Object | Select-Object -ExpandProperty Count
    
    #Get a count of Servers
    $ServerCount      = $Devices | Where-Object {$_.customer_id -eq $customer.id -and $_.role -eq "WINDOWS_SERVER"} | Measure-Object | Select-Object -ExpandProperty Count
    
    #Get a count of Servers that have a special name that indicated it's a backup server
    $BDRCount         = $Devices | Where-Object {$_.customer_id -eq $customer.id -and $_.role -eq "WINDOWS_SERVER" -and ($_.dns_name -like "*BDR*" -or $_.dns_name -like "*-RR*" -or $_.dns_name -like "*APPASSURE*" -or $_.dns_name -like "*NAS*")} | Measure-Object | Select-Object -ExpandProperty Count
    
    #Subtract the BDRs from the Server count
    $ServerCount      = $ServerCount - $BDRCount

    #Create a Custom Object with all of this information
    $Info = [PSCustomObject] @{
        "Customer" = $Customer.name
        "Workstaion Count" = $WorkstationCount
        "Server Count" = $ServerCount
        "BDR Count" = $BDRCount
    }

    #Add the object to the blank array
    $FinalOutput += $Info

}

#Set Variables for Send-MailMessage
$SMTPServer = ""
$From       = ""
$To         = ""
$Subjet     = "Machine Counts"
$Body       = $FinalOutput | Sort-Object Customer | ConvertTo-Html | Out-String

#Send the Email With the Counts
Send-MailMessage -SmtpServer $SMTPServer -From $From -To $To -Subject $Subjet -Body $Body -BodyAsHtml